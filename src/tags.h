#ifndef TAGS_H_UNCLUDED
#define TAGS_H_UNCLUDED

#include <iostream>

// cherry tags
#define CH_CHERRYTREE "cherrytree"
#define CH_NODE "node"
#define CH_RICH_TEXT "rich_text"
#define CH_STYLE "style"


class Tag
{
 public:
    virtual void initTag ();
    virtual void compose ();
    // preview: cherrytree /home/bstepankova/Documents/training/notes_cherry_tree/notes.ctd
 private:
    const std::string left_angle = "<";
    const std::string right_angle = ">";
    const std::string slash = "/";
    const std::string rich_text = "rich_text";
    std::string new_line = "";
};
#endif
