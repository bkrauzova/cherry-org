#include "tags.h"

void Tag::initTag ()
{
    std::cout << "<?xml version=\"1.0\" ?>";
}

void Tag::compose ()
{
    std::cout << left_angle << rich_text << slash << right_angle << std::endl;
}
