/*
 * Copyright (c) 2021 Barbora Krauzova
 *
 * This is a free software, it can be modified and/or
 * redistributed under MIT license. See LICENSE file or
 * https://opensource.org/licenses/MIT for details.
 *
 */

#include <fstream>
#include <string>
#include <vector>
#include <iostream>
#include <ctime>

#define SEPARATOR '/'

#define NR_OF_MR 11
#define ORG_MARKER 0
#define CHERRY_MARKER 1

bool isNumberedList (const std::string &tag)
{
    size_t pos = tag.find (".");

    if (pos == std::string::npos) {
        return false;
    }

    return std::is_digit (tag.substr (0, pos));
}

bool isPairTag (const std::string &tag)
{
    if (tag == "*" ||
        tag == "/" ||
        tag == "~" ||
        tag == "_" ||
        tag == "+" ||
        tag == "="
    ) {
        return true;
    } else {
        return false;
    }

}

bool isHeader (std::string &tag)
{
    if (tag == "* "   ||
        tag == "** "  ||
        tag == "*** " ||
        tag == "**** "
    ) {
        return true;
    } else {
        return false;
    }
}

const std::string markerMapping [2][NR_OF_MR] = {
    // org
    {"* ", "=", "1.", "+ ", "-", "#", "*", "/",  "_", "~", "+"},
    // cherry
    //                      bullet
    {"h", "codebox", "1.", "\u2022", "\u2022", "COMMENT", "weight=\"heavy\"", "style=\"italic\"", "underline=\"single\"", "VERBATIM", "strikethrough=\"true\""}
};

void help ()
{
    std::cout << "\n";
    std::cout << "Usage:\n";
    std::cout << "\torg2cherry [absolute_path]\n";
    std::cout << "\n";
}

bool checkOrg (std::string file)
{

    return false;
}

// RETURNS full path and file name (without extension)
std::string getFilename (std::string fullPath)
{
    size_t pos = fullPath.find_last_of (SEPARATOR);
    if (pos == std::string::npos) {
        pos = 0;
    }
    std::string fileName = fullPath.substr (pos + 1);
    std::string path = fullPath.substr (0, pos);
    pos = fileName.find (".");

    if (pos == std::string::npos) {
        return path + "/" +fileName;
    }

    return path + "/" + fileName.substr (0, pos);

}

std::string xmlParse (std::string marker, std::string text)
{
    //TODO

    return "nothing";
}

std::string finishCherry ()
{
    return "\t<\node>\n</cherrytree>\n";
}

std::string initCherry ()
{
    std::string timeNow = std::to_string (std::time (nullptr));
    // TODO SAVE replaces by timestamp before saving, NODE_NAME
    return "<?xml version=\"1.0\" ?>\n<cherrytree>\n\t<node custom_icon_id=\"0\" foreground=\"\" is_bold=\"False\" name=\"NODE_NAME\" prog_lang=\"custom-colors\" readonly=\"False\" tags=\"\" ts_creation=\"" + timeNow + "\"" + " ts_lastsave=\"SAVE\" unique_id=\"1\">\n";
}

// open cherry tree to check the result
void filePreview (std::string fileName)
{
    char program [50] = {0};

    sprintf (program, "%s %s.ctd", "cherrytree", fileName.c_str ());
    system (program);
}

int main (int argc, char **argv)
{
    if (argc != 2) {
        std::cout << "Error incorrect number of arguments\n";
        help ();

        exit (1);
    }

    std::string fileName = getFilename (argv [1]);
    if (fileName.empty ()) {
        std::cout << "Error retrieving file name." << std::endl;
    }

    std::ifstream in; // org
    std::ofstream out; // cherry

    try {
        in.open (argv [1]);
        out.open (fileName + ".ctd");

        if (!in.is_open () || !out.is_open ()) {
            exit (1);
        }
    }
    catch (const std::exception &e) {
        std::cout << "Error: " << e.what () << std::endl;
    }

    std::string line;
    std::string marker;
    std::string text;

    out << initCherry ();

    while (getline (in, line)) {
        for (size_t i = 0; i < NR_OF_MR; i++) {

            marker = markerMapping [ORG_MARKER][i];
            size_t pos = line.find (marker);

            if (pos != std::string::npos) {
                text = line.substr (pos, line.length () - 1);
                out << xmlParse (marker, text);
                break;
            }
        } // for

        xmlParse ("", text);

    } // while


    out << finishCherry ();

    in.close ();
    out.close ();

    filePreview (fileName);

    return 0;
}
