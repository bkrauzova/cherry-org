SRC = ./src

CXX       = g++
CXXFLAGS  = -std=c++11 -g -Wall -fPIC -Werror -Weffc++
CXX_FILES = $(SRC)/org2cherry.cc
BIN1      = cherry2org
BIN2      = org2cherry

all: $(BIN2)
	@echo ------- Compiling -------

help:
	@echo -e ""
	@echo -e "make help 		shows help"
	@echo -e "make all		builds all files"
	@echo -e "make clean		cleans project"
	@echo -e ""


org2cherry:
	$(CXX) $(CXXFLAGS) $(CXX_FILES) -o $(BIN2)

clean:
	@if [ -e $(BIN1) ]; then \
		rm -f $(BIN1); \
	fi

	@if [ -e $(BIN2) ]; then \
		rm -f $(BIN2); \
	fi
